//1
let numbers = [5, 25, 89, 120, 36];
numbers.push("javascript", "python");
numbers.unshift("html", "css");
console.log(numbers.length);
numbers.shift();
numbers.pop();
console.log(numbers);

//2
let fruits = ["ფორთოხალი", "ბანანი", "მსხალი"];
console.log(fruits.length);
fruits.push("ვაშლი", "ანანასი");
fruits.unshift("საზამთრო");
console.log(fruits.length);
fruits.splice(2, 0, "მანგო");
fruits.shift();
fruits.pop();
console.log(fruits.length);

//3
let student = {
  name: "John",
  age: 28,
  faculty: "Philosophy",
};
for (let key of Object.keys(student)) {
  console.log(key);
}

//4
for (let value of Object.values(student)) {
  console.log(value);
}

//5
let array = [12, 25, 3, 6, 8, 14, 7, 23];
let NewArray = array.map((x) => x / 3);
console.log(NewArray);

//6
let arr = [
  "hello",
  125,
  "javascript",
  "html",
  43,
  "css",
  "scss",
  "bootstrap",
  88,
  59,
  "python",
];
let newarr = arr.filter((num) => typeof num == "number");
console.log(newarr);
